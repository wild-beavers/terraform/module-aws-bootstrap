# Terraform module: bootstrap S3

Bootstrap terraform AWS S3 repository.

This module should be used in a client-specific bootstrap terraform module.

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| terraform | >= 1.0.0 |
| aws | >= 3.35 |

## Providers

| Name | Version |
|------|---------|
| aws | >= 3.35 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| cloudtrail\_s3\_bucket | gitlab.com/wild-beavers/module-aws-bucket-s3/aws | ~> 9 |

## Resources

| Name | Type |
|------|------|
| [aws_dynamodb_table.terraform_statefile](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/dynamodb_table) | resource |
| [aws_iam_group_policy_attachment.groups](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_group_policy_attachment) | resource |
| [aws_iam_policy.terraform_bucket](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_policy.terraform_subdirectory](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_role.terraform_bucket](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role.terraform_bucket_teams](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy_attachment.terraform_bucket](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.terraform_bucket_teams](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_kms_key.terraform_bucket](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_key) | resource |
| [aws_kms_key.terraform_statefile_dynamodb](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_key) | resource |
| [aws_s3_bucket.terraform_bucket](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) | resource |
| [aws_s3_bucket_policy.terraform_bucket](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_policy) | resource |
| [aws_s3_bucket_public_access_block.terraform_bucket](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_public_access_block) | resource |
| [aws_caller_identity.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |
| [aws_iam_group.groups](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_group) | data source |
| [aws_iam_policy_document.account_assume_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.allow_all](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.bucket_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.cloudtrail_s3_bucket](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.cloudtrail_s3_bucket_kms](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.cloudtrail_s3_bucket_trail](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.cloudtrail_s3_bucket_trail_kms](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.limited_access](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_partition.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/partition) | data source |
| [aws_region.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/region) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| allowed\_aws\_user\_id\_patterns | List of AWS user ID patterns that will be allowed to access the bucket.<br/>All ID's that do not match that pattern will be denied access to the bucket.<br/>If `attach_subdirectories_policy_to_existing_groups` is true, these will automatically be allowed.<br/>[More Information](https://aws.amazon.com/blogs/security/how-to-restrict-amazon-s3-bucket-access-to-a-specific-iam-role/) | `list(string)` | `[]` | no |
| attach\_subdirectories\_policy\_to\_existing\_groups | If true, the module will search for existing groups by the names passed in var.subdirectories\_by\_teams and attach the specific policy to the specific group. | `bool` | `false` | no |
| cloudtrail\_s3\_bucket\_enabled | Whether to enable the S3 CloudTrail audit S3 bucket or not. | `bool` | `false` | no |
| cloudtrail\_s3\_bucket\_iam\_policy\_entity\_arns | Restrict access to the key to the given IAM entities (roles or users). Wildcards are allowed. Allowed keys are `ro`, `rw`, `rwd`, `audit` or `full`, each specifying the permission scope for entities inside them. If the same entity is inside multiple scope, the least privileged scope takes precedence over the most privileged. | `map(map(string))` | `{}` | no |
| cloudtrail\_s3\_bucket\_kms\_key\_alias | Alias of the CloudTrail audit S3 bucket KMS key to create. Conflict with “var.cloudtrail\_audit\_s3\_bucket\_kms\_key\_id” if “var.cloudtrail\_audit\_s3\_bucket\_kms\_key\_use\_external\_key” is set. | `string` | `null` | no |
| cloudtrail\_s3\_bucket\_kms\_key\_deletion\_window\_in\_days | The waiting period, specified in number of days. After the waiting period ends, AWS KMS deletes the KMS key. Must be between “7” and “30”. Default to “7”. | `number` | `null` | no |
| cloudtrail\_s3\_bucket\_kms\_key\_id | KMS key of the CloudTrail audit S3 bucket. Conflict with “var.cloudtrail\_audit\_s3\_bucket\_kms\_key\_alias” if “var.cloudtrail\_audit\_s3\_bucket\_kms\_key\_use\_external\_key” is set. | `string` | `null` | no |
| cloudtrail\_s3\_bucket\_kms\_key\_policy\_json | Additional policy to attach to the KMS key, merged with a baseline internal policy. | `string` | `null` | no |
| cloudtrail\_s3\_bucket\_kms\_key\_rotation\_enabled | Whether to automatically rotate the KMS key linked to the S3 or not. | `bool` | `true` | no |
| cloudtrail\_s3\_bucket\_kms\_key\_tags | Tags to be merged with all CloudTrail audit S3 bucket KMS key resources | `map(string)` | `{}` | no |
| cloudtrail\_s3\_bucket\_name | Name of the CloudTrail audit S3 bucket. | `string` | `""` | no |
| cloudtrail\_s3\_bucket\_object\_lifecycle\_retention\_days | The number of days to keep CloudTrail audit logs. Older objects will be automatically removed. | `number` | `15` | no |
| cloudtrail\_s3\_bucket\_object\_lifecycle\_retention\_mode | The retention mode. Must be one of “GOVERNANCE” or “COMPLIANCE”. | `string` | `"COMPLIANCE"` | no |
| cloudtrail\_s3\_bucket\_policy\_json | A valid bucket policy JSON document to be applied to the CloudTrail audit S3 bucket. Careful, this comes in complement of default IAM created by this module. | `string` | `null` | no |
| cloudtrail\_s3\_bucket\_tags | Tags to be merged with all CloudTrail audit S3 bucket resources | `map(string)` | `{}` | no |
| cloudtrail\_trails\_account\_ids | Set of account IDs allowed to access to CloudTrail audit S3 bucket | `set(string)` | `[]` | no |
| force\_destroy | Whether to force destroy resources containing data or not | `bool` | `false` | no |
| subdirectories\_by\_teams | Map of objects to allow a sub-folder to a team to access given directories. Keys are free values, value is an object where:<br/>* name: (required, string) team name<br/>* directory: (required, string) allowed sub-directory | <pre>map(object({<br/>    group_name = string<br/>    directory  = string<br/>  }))</pre> | `{}` | no |
| tags | Additional tags to add to all AWS resources. Will be merged with the default tags provided by this module. | `map(string)` | `{}` | no |
| terraform\_statefile\_dynamodb\_enabled | Whether to create DynamoDB for Terraform State Lock or not. | `bool` | `true` | no |
| terraform\_statefile\_s3\_bucket\_prefix | Prefix of the bucket that will contain terraform state files. | `string` | `"vendor-tfstate"` | no |
| trusted\_aws\_accounts | List of additional AWS account which will be trusted to assume the s3 role. | `list(string)` | `[]` | no |
| trusted\_aws\_iam\_identifiers | List of additional AWS IAM identifiers which will be trusted to assume the s3 role. (ex: A specific role in an other account) | `list(string)` | `[]` | no |
| vendor\_prefix | Initials of the company which the project is bootstrapped. | `string` | `"vendor"` | no |

## Outputs

| Name | Description |
|------|-------------|
| cloudtrail\_aws\_iam\_policies | n/a |
| cloudtrail\_aws\_kms\_alias | n/a |
| cloudtrail\_aws\_kms\_key | n/a |
| cloudtrail\_aws\_s3\_bucket | n/a |
| cloudtrail\_kms\_aws\_iam\_policies | n/a |
| dynamodb\_arn | n/a |
| dynamodb\_id | n/a |
| dynamodb\_name | n/a |
| iam\_policy\_arns | n/a |
| iam\_policy\_ids | n/a |
| iam\_role\_arns | n/a |
| iam\_role\_ids | n/a |
| iam\_role\_unique\_ids | n/a |
| kms\_arn | n/a |
| kms\_id | n/a |
| s3\_arn | n/a |
| s3\_bucket\_domain\_name | n/a |
| s3\_id | n/a |
| s3\_region | n/a |
<!-- END_TF_DOCS -->
