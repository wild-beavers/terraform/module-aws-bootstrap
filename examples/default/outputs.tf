####
# Default
####

output "default" {
  value = module.default
}

####
# Restricted
####

output "restricted" {
  value = module.restricted
}
