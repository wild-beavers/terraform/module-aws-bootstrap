####
# Context
####

locals {
  prefix          = "${random_string.start_letter.result}${random_string.this.result}"
  cloudtrail_name = "${local.prefix}tftesttrail"
}

data "aws_caller_identity" "current" {}

resource "random_string" "start_letter" {
  length  = 1
  upper   = false
  special = false
  numeric = false
}

resource "random_string" "this" {
  length  = 3
  upper   = false
  special = false
}

resource "aws_iam_group" "test" {
  name = "${local.prefix}tftest"
}

resource "aws_iam_user" "test" {
  name = "${local.prefix}tftest"
}

resource "aws_iam_group_membership" "member" {
  group = aws_iam_group.test.id
  name  = aws_iam_user.test.id
  users = [aws_iam_user.test.id]
}

resource "aws_cloudtrail" "example" {
  name                          = local.cloudtrail_name
  s3_bucket_name                = module.default.cloudtrail_aws_s3_bucket.id
  kms_key_id                    = module.default.cloudtrail_aws_kms_key.arn
  enable_log_file_validation    = true
  enable_logging                = true
  include_global_service_events = true
  is_multi_region_trail         = true
  s3_key_prefix                 = "myprefix"

  event_selector {
    read_write_type           = "All"
    include_management_events = true

    data_resource {
      type   = "AWS::S3::Object"
      values = ["arn:aws:s3"]
    }
  }

  event_selector {
    read_write_type           = "All"
    include_management_events = true

    data_resource {
      type   = "AWS::DynamoDB::Table"
      values = ["arn:aws:dynamodb"]
    }
  }

  event_selector {
    read_write_type           = "All"
    include_management_events = true

    data_resource {
      type   = "AWS::Lambda::Function"
      values = ["arn:aws:lambda"]
    }
  }

  insight_selector {
    insight_type = "ApiCallRateInsight"
  }

  insight_selector {
    insight_type = "ApiErrorRateInsight"
  }

  depends_on = [module.default]
}

####
# Default
####

module "default" {
  source = "../../"

  vendor_prefix                                   = "${local.prefix}default"
  terraform_statefile_s3_bucket_prefix            = "${local.prefix}default"
  attach_subdirectories_policy_to_existing_groups = true
  force_destroy                                   = true
  subdirectories_by_teams = {
    test = {
      group_name = aws_iam_group.test.name
      directory  = "test"
    }
  }

  cloudtrail_s3_bucket_enabled                         = true
  cloudtrail_s3_bucket_kms_key_alias                   = "${local.prefix}tftestcloudtrailaudit"
  cloudtrail_s3_bucket_kms_key_deletion_window_in_days = 7
  cloudtrail_s3_bucket_kms_key_rotation_enabled        = true
  cloudtrail_s3_bucket_name                            = "${local.prefix}tftestcloudtrailaudit"
  // We use "GOVERNANCE" in test to avoid locking the S3 bucket. This not safe for production and you should always consider “COMPLIANCE”
  cloudtrail_s3_bucket_object_lifecycle_retention_mode = "GOVERNANCE"
  cloudtrail_s3_bucket_object_lifecycle_retention_days = 1
  cloudtrail_s3_bucket_iam_policy_entity_arns = {
    full = { test = aws_iam_user.test.arn }
  }
  cloudtrail_trails_account_ids = toset([data.aws_caller_identity.current.account_id])

  depends_on = [aws_iam_group_membership.member]

  providers = {
    aws.replica = aws
  }
}

module "restricted" {
  source = "../../"

  vendor_prefix                        = "${local.prefix}restricted"
  terraform_statefile_s3_bucket_prefix = "${local.prefix}restricted"
  terraform_statefile_dynamodb_enabled = false

  providers = {
    aws.replica = aws
  }
}
