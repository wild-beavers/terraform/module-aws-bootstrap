output "s3_id" {
  value = aws_s3_bucket.terraform_bucket.id
}

output "s3_arn" {
  value = aws_s3_bucket.terraform_bucket.arn
}

output "s3_bucket_domain_name" {
  value = aws_s3_bucket.terraform_bucket.bucket_domain_name
}

output "s3_region" {
  value = aws_s3_bucket.terraform_bucket.region
}

output "kms_id" {
  value = aws_kms_key.terraform_bucket.key_id
}

output "kms_arn" {
  value = aws_kms_key.terraform_bucket.arn
}

output "dynamodb_arn" {
  value = var.terraform_statefile_dynamodb_enabled ? aws_dynamodb_table.terraform_statefile["0"].arn : null
}

output "dynamodb_id" {
  value = var.terraform_statefile_dynamodb_enabled ? aws_dynamodb_table.terraform_statefile["0"].id : null
}

output "dynamodb_name" {
  value = var.terraform_statefile_dynamodb_enabled ? aws_dynamodb_table.terraform_statefile["0"].name : null
}

#####
# IAM Policies & Roles
#####

output "iam_policy_ids" {
  value = merge(
    {
      all = aws_iam_policy.terraform_bucket.id,
    },
    {
      for key, policy in aws_iam_policy.terraform_subdirectory : key => policy.id
    }
  )
}

output "iam_policy_arns" {
  value = merge(
    {
      all = aws_iam_policy.terraform_bucket.arn,
    },
    {
      for key, policy in aws_iam_policy.terraform_subdirectory : key => policy.arn
    }
  )
}

output "iam_role_ids" {
  value = merge(
    {
      all = aws_iam_role.terraform_bucket.id
    },
    {
      for key, role in aws_iam_role.terraform_bucket_teams : key => role.id
    }
  )
}

output "iam_role_arns" {
  value = merge(
    {
      all = aws_iam_role.terraform_bucket.arn
    },
    {
      for key, role in aws_iam_role.terraform_bucket_teams : key => role.arn
    }
  )
}

output "iam_role_unique_ids" {
  value = merge(
    {
      all : aws_iam_role.terraform_bucket.unique_id
    },
    {
      for key, role in aws_iam_role.terraform_bucket_teams : key => role.unique_id
    }
  )
}
