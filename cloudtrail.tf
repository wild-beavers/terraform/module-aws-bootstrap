####
# Variables
####

variable "cloudtrail_s3_bucket_enabled" {
  description = "Whether to enable the S3 CloudTrail audit S3 bucket or not."
  type        = bool
  default     = false
  nullable    = false
}

variable "cloudtrail_s3_bucket_name" {
  description = "Name of the CloudTrail audit S3 bucket."
  type        = string
  default     = ""
  nullable    = false

  validation {
    condition     = var.cloudtrail_s3_bucket_name == "" || can(regex("^[a-z0-9][a-z0-9\\.\\-]{2,61}[a-z0-9]$", var.cloudtrail_s3_bucket_name))
    error_message = "“var.cloudtrail_s3_bucket_name” must match “^[a-z0-9][a-z0-9\\.\\-]{2,61}[a-z0-9]$”."
  }
}

variable "cloudtrail_s3_bucket_policy_json" {
  description = "A valid bucket policy JSON document to be applied to the CloudTrail audit S3 bucket. Careful, this comes in complement of default IAM created by this module."
  type        = string
  default     = null

  validation {
    condition     = var.cloudtrail_s3_bucket_policy_json == null || can(jsondecode(var.cloudtrail_s3_bucket_policy_json))
    error_message = "“var.cloudtrail_s3_bucket_policy_json” must be a valid JSON document."
  }
}

variable "cloudtrail_s3_bucket_kms_key_id" {
  description = "KMS key of the CloudTrail audit S3 bucket. Conflict with “var.cloudtrail_audit_s3_bucket_kms_key_alias” if “var.cloudtrail_audit_s3_bucket_kms_key_use_external_key” is set."
  type        = string
  default     = null

  validation {
    condition     = var.cloudtrail_s3_bucket_kms_key_id == null || (can(regex("^[a-f0-9]{8}\\-([a-f0-9]{4}\\-){3}[a-f0-9]{12}$", var.cloudtrail_s3_bucket_kms_key_id)) || can(regex("^mrk\\-[a-f0-9]{32}$", var.cloudtrail_s3_bucket_kms_key_id)) || can(regex("^alias/[a-zA-Z0-9:/_-]{1,256}$", var.cloudtrail_s3_bucket_kms_key_id)))
    error_message = "“var.cloudtrail_s3_bucket_kms_key_id” must match one of “^mrk\\-[a-f0-9]{32}$”, “^[a-f0-9]{8}\\-([a-f0-9]{4}\\-){3}[a-f0-9]{12}$” or “^alias/[a-zA-Z0-9:/_-]{1,256}$”."
  }
}

variable "cloudtrail_s3_bucket_kms_key_alias" {
  description = "Alias of the CloudTrail audit S3 bucket KMS key to create. Conflict with “var.cloudtrail_audit_s3_bucket_kms_key_id” if “var.cloudtrail_audit_s3_bucket_kms_key_use_external_key” is set."
  type        = string
  default     = null

  validation {
    condition     = var.cloudtrail_s3_bucket_kms_key_alias == null || (can(regex("^[a-zA-Z0-9:/_-]{1,256}$", var.cloudtrail_s3_bucket_kms_key_alias)) && !startswith(coalesce(var.cloudtrail_s3_bucket_kms_key_alias, "false"), "alias/"))
    error_message = "“var.cloudtrail_s3_bucket_kms_key_alias” must match “^(?!alias/)[a-zA-Z0-9:/_-]{1,256}$”."
  }
}

variable "cloudtrail_s3_bucket_kms_key_policy_json" {
  description = "Additional policy to attach to the KMS key, merged with a baseline internal policy."
  type        = string
  default     = null

  validation {
    condition     = var.cloudtrail_s3_bucket_kms_key_policy_json == null || can(jsondecode(var.cloudtrail_s3_bucket_kms_key_policy_json))
    error_message = "“var.cloudtrail_s3_bucket_kms_key_policy_json” must be a valid JSON document."
  }
}

variable "cloudtrail_s3_bucket_kms_key_rotation_enabled" {
  description = "Whether to automatically rotate the KMS key linked to the S3 or not."
  type        = bool
  default     = true
  nullable    = false
}

variable "cloudtrail_s3_bucket_kms_key_deletion_window_in_days" {
  description = "The waiting period, specified in number of days. After the waiting period ends, AWS KMS deletes the KMS key. Must be between “7” and “30”. Default to “7”."
  type        = number
  default     = null

  validation {
    condition     = var.cloudtrail_s3_bucket_kms_key_deletion_window_in_days == null || can(var.cloudtrail_s3_bucket_kms_key_deletion_window_in_days > 7 && var.cloudtrail_s3_bucket_kms_key_deletion_window_in_days < 30)
    error_message = "“var.cloudtrail_s3_bucket_kms_key_deletion_window_in_days” must be a number between “7” and “30”."
  }
}

variable "cloudtrail_s3_bucket_iam_policy_entity_arns" {
  description = "Restrict access to the key to the given IAM entities (roles or users). Wildcards are allowed. Allowed keys are `ro`, `rw`, `rwd`, `audit` or `full`, each specifying the permission scope for entities inside them. If the same entity is inside multiple scope, the least privileged scope takes precedence over the most privileged."
  type        = map(map(string))
  default     = {}
  nullable    = false

  validation {
    condition = (!contains([for key, arns in var.cloudtrail_s3_bucket_iam_policy_entity_arns :
      (
        contains(["ro", "rw", "rwd", "audit", "full"], key) &&
        (
          !contains([for arn in values(arns) :
            (
              arn == null || can(regex("^arn:aws(-us-gov|-cn)?:(iam|sts):(([a-z]{2}-[a-z]{4,10}-[1-9]{1})?|\\*):([0-9]{12}|aws|\\*):(assumed-role|role|user|federated-user)/[a-zA-Z0-9+=,_:\\*\\./@{}'\\$\\?-]+$", arn))
            )
          ], false)
        )
      )
    ], false))
    error_message = "One or more “var.cloudtrail_s3_bucket_iam_policy_entity_arns” are invalid."
  }
}

variable "cloudtrail_s3_bucket_kms_key_tags" {
  description = "Tags to be merged with all CloudTrail audit S3 bucket KMS key resources"
  type        = map(string)
  default     = {}
  nullable    = false
}

variable "cloudtrail_s3_bucket_tags" {
  description = "Tags to be merged with all CloudTrail audit S3 bucket resources"
  type        = map(string)
  default     = {}
  nullable    = false
}

variable "cloudtrail_s3_bucket_object_lifecycle_retention_mode" {
  description = "The retention mode. Must be one of “GOVERNANCE” or “COMPLIANCE”."
  type        = string
  default     = "COMPLIANCE"
  nullable    = false

  validation {
    condition     = contains(["GOVERNANCE", "COMPLIANCE"], var.cloudtrail_s3_bucket_object_lifecycle_retention_mode)
    error_message = "“var.cloudtrail_s3_bucket_object_lifecycle_retention_mode” must be one of “GOVERNANCE” or “COMPLIANCE”."
  }
}

variable "cloudtrail_s3_bucket_object_lifecycle_retention_days" {
  description = "The number of days to keep CloudTrail audit logs. Older objects will be automatically removed."
  type        = number
  default     = 15
  nullable    = false

  validation {
    condition     = var.cloudtrail_s3_bucket_object_lifecycle_retention_days > 0
    error_message = "“var.cloudtrail_s3_bucket_object_lifecycle_retention_days” must be greater that 0."
  }
}

variable "cloudtrail_trails_account_ids" {
  description = "Set of account IDs allowed to access to CloudTrail audit S3 bucket"
  type        = set(string)
  default     = []
  nullable    = false

  validation {
    condition = alltrue([for v in var.cloudtrail_trails_account_ids :
      can(regex("^\\d{12}$", v))
    ])
    error_message = "One or more “var.cloudtrail_trails_account_ids” don't match “^\\d{12}$”."
  }
}

####
# Resources and data
####

locals {
  cloudtrail_s3_bucket_tags = merge(
    var.tags,
    local.tags,
    var.cloudtrail_s3_bucket_tags
  )
  cloudtrail_trails_arns = distinct([for account_id in var.cloudtrail_trails_account_ids :
    "arn:${data.aws_partition.current.partition}:cloudtrail:${data.aws_region.current.name}:${account_id}:trail/*"
  ])
  //For S3 internal policy cycle reason, ARN is precomputed
  cloudtrail_s3_bucket_arn_computed = "arn:${data.aws_partition.current.partition}:s3:::${var.cloudtrail_s3_bucket_name}"

  cloudtrail_s3_bucket_iam_policy_entity_required_arns = { full = { caller = local.caller_identity } }

  cloudtrail_s3_bucket_iam_policy_entity_arns = { for scope in distinct(concat(keys(var.cloudtrail_s3_bucket_iam_policy_entity_arns), keys(local.cloudtrail_s3_bucket_iam_policy_entity_required_arns))) : scope =>
    merge(
      lookup(var.cloudtrail_s3_bucket_iam_policy_entity_arns, scope, {}),
      lookup(local.cloudtrail_s3_bucket_iam_policy_entity_required_arns, scope, {})
    )
  }
}

module "cloudtrail_s3_bucket" {
  source  = "gitlab.com/wild-beavers/module-aws-bucket-s3/aws"
  version = "~> 9"

  for_each = var.cloudtrail_s3_bucket_enabled ? { 0 = 0 } : {}

  buckets = {
    cloudtrail = {
      name                    = var.cloudtrail_s3_bucket_name
      block_public_acls       = true
      block_public_policy     = true
      force_destroy           = var.force_destroy
      ignore_public_acls      = true
      restrict_public_buckets = true
      tags                    = { Description = "Bucket for CloudTrail audit logs." },
      sse_enabled             = true
      bucket_object_ownership = "BucketOwnerEnforced"
    }
  }

  kms_key_id = var.cloudtrail_s3_bucket_kms_key_id
  kms_key = {
    alias                   = var.cloudtrail_s3_bucket_kms_key_alias
    description             = "For audit S3 bucket ${var.cloudtrail_s3_bucket_name}."
    policy_json             = data.aws_iam_policy_document.cloudtrail_s3_bucket_kms["0"].json
    rotation_enabled        = var.cloudtrail_s3_bucket_kms_key_rotation_enabled
    deletion_window_in_days = var.cloudtrail_s3_bucket_kms_key_deletion_window_in_days
    tags = merge(
      local.cloudtrail_s3_bucket_tags,
      var.cloudtrail_s3_bucket_kms_key_tags
    )
  }

  iam_policy_source_policy_documents = [data.aws_iam_policy_document.cloudtrail_s3_bucket["0"].json]
  // "086441151436" seems to be AWS account to check S3 from cloudtrail service
  // Without the account, creating a cloudtrail seems forbidden
  iam_policy_restrict_by_account_ids = concat(["086441151436"], tolist(var.cloudtrail_trails_account_ids))

  versioning_configurations = {
    cloudtrail = {
      status = true
    }
  }

  object_lock_configurations = {
    cloudtrail = {
      rule = {
        default_retention = {
          mode = var.cloudtrail_s3_bucket_object_lifecycle_retention_mode
          days = var.cloudtrail_s3_bucket_object_lifecycle_retention_days
        }
      }
    }
  }

  lifecycle_configuration_default_rules = [
    {
      id     = "expired_objects"
      status = "Enabled"
      noncurrent_version_expiration = {
        noncurrent_days = var.cloudtrail_s3_bucket_object_lifecycle_retention_days + 1
      }
      expiration = {
        days = var.cloudtrail_s3_bucket_object_lifecycle_retention_days + 1
      }
      abort_incomplete_multipart_upload = {
        days_after_initiation = 1
      }
    },
    {
      id     = "delete_expired_objects"
      status = "Enabled"
      expiration = {
        expired_object_delete_marker = true
      }
    }
  ]

  iam_policy_entity_arns = local.cloudtrail_s3_bucket_iam_policy_entity_arns
  iam_policy_source_arns = {
    full = { for v in local.cloudtrail_trails_arns : v => v }
  }
  iam_policy_export_json = true

  tags = local.cloudtrail_s3_bucket_tags

  providers = {
    aws.replica = aws.replica
  }
}

data "aws_iam_policy_document" "cloudtrail_s3_bucket" {
  for_each = var.cloudtrail_s3_bucket_enabled ? { 0 = 0 } : {}

  source_policy_documents = compact(concat(
    [data.aws_iam_policy_document.cloudtrail_s3_bucket_trail["0"].json],
    [var.cloudtrail_s3_bucket_policy_json],
  ))
}

data "aws_iam_policy_document" "cloudtrail_s3_bucket_kms" {
  for_each = var.cloudtrail_s3_bucket_enabled ? { 0 = 0 } : {}

  source_policy_documents = compact(concat(
    [data.aws_iam_policy_document.cloudtrail_s3_bucket_trail_kms["0"].json],
    [var.cloudtrail_s3_bucket_kms_key_policy_json],
  ))
}

data "aws_iam_policy_document" "cloudtrail_s3_bucket_trail" {
  # Statements here comes from: https://docs.aws.amazon.com/awscloudtrail/latest/userguide/create-s3-bucket-policy-for-cloudtrail.html
  for_each = var.cloudtrail_s3_bucket_enabled ? { 0 = 0 } : {}

  statement {
    sid    = "AWSCloudTrail"
    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = ["cloudtrail.amazonaws.com"]
    }
    actions   = ["s3:GetBucketAcl"]
    resources = [local.cloudtrail_s3_bucket_arn_computed]
    condition {
      test     = "ArnLike"
      variable = "aws:SourceArn"
      values   = local.cloudtrail_trails_arns
    }
  }

  statement {
    sid    = "AWSCloudTrailWrite"
    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = ["cloudtrail.amazonaws.com"]
    }
    actions   = ["s3:PutObject"]
    resources = ["${local.cloudtrail_s3_bucket_arn_computed}/AWSLogs/*"]
    condition {
      test     = "ArnLike"
      variable = "aws:SourceArn"
      values   = local.cloudtrail_trails_arns
    }
    condition {
      test     = "StringEquals"
      variable = "s3:x-amz-acl"
      values   = ["bucket-owner-full-control"]
    }
  }
}

data "aws_iam_policy_document" "cloudtrail_s3_bucket_trail_kms" {
  for_each = var.cloudtrail_s3_bucket_enabled ? { 0 = 0 } : {}

  statement {
    sid    = "CloudtrailEncrypt"
    effect = "Allow"
    principals {
      identifiers = ["cloudtrail.amazonaws.com"]
      type        = "Service"
    }
    actions   = ["kms:GenerateDataKey*"]
    resources = ["*"]
    condition {
      test     = "ArnLike"
      values   = local.cloudtrail_trails_arns
      variable = "aws:SourceArn"
    }
    condition {
      test     = "ArnLike"
      values   = local.cloudtrail_trails_arns
      variable = "kms:EncryptionContext:aws:cloudtrail:arn"
    }
  }

  statement {
    sid    = "CloudTrailDescribeKey"
    effect = "Allow"
    principals {
      identifiers = ["cloudtrail.amazonaws.com"]
      type        = "Service"
    }
    actions   = ["kms:DescribeKey"]
    resources = ["*"]
  }

  statement {
    sid    = "CloudtrailDecrypt"
    effect = "Allow"
    principals {
      identifiers = ["*"]
      type        = "AWS"
    }
    actions   = ["kms:Decrypt", "kms:ReEncryptFrom"]
    resources = ["*"]
    condition {
      test     = "StringEquals"
      values   = distinct(var.cloudtrail_trails_account_ids)
      variable = "kms:CallerAccount"
    }
    condition {
      test     = "ArnLike"
      values   = local.cloudtrail_trails_arns
      variable = "kms:EncryptionContext:aws:cloudtrail:arn"
    }
  }
}

####
# Outputs
####

output "cloudtrail_aws_kms_key" {
  value = var.cloudtrail_s3_bucket_enabled ? module.cloudtrail_s3_bucket["0"].aws_kms_key : null
}

output "cloudtrail_aws_kms_alias" {
  value = var.cloudtrail_s3_bucket_enabled ? module.cloudtrail_s3_bucket["0"].aws_kms_alias : null
}

output "cloudtrail_kms_aws_iam_policies" {
  value = var.cloudtrail_s3_bucket_enabled ? module.cloudtrail_s3_bucket["0"].kms_aws_iam_policies : null
}

output "cloudtrail_aws_s3_bucket" {
  value = var.cloudtrail_s3_bucket_enabled ? module.cloudtrail_s3_bucket["0"].aws_s3_buckets["cloudtrail"] : null
}

output "cloudtrail_aws_iam_policies" {
  value = var.cloudtrail_s3_bucket_enabled ? module.cloudtrail_s3_bucket["0"].aws_iam_policies : null
}
