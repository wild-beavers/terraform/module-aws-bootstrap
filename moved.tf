moved {
  from = aws_dynamodb_table.this
  to   = aws_dynamodb_table.terraform_statefile["0"]
}

moved {
  from = aws_kms_key.terraform_dynamodb
  to   = aws_kms_key.terraform_statefile_dynamodb
}
